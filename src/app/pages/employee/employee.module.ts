import { EmployeeRoutingModule } from './employee.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './employee.component';
import { EmployeeService } from '../../services/employee.service';
import {MatExpansionModule} from '@angular/material/expansion';
import { FindAgePipe } from '../../pipe/age.pipe';
import { PvfPipe } from '../../pipe/pvf.pipe';
import { LayoutModule } from '@angular/cdk/layout';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import { EmployeeInfoComponent } from './employee-info/employee-info.component';
@NgModule({
  declarations: [
    EmployeeComponent,
    FindAgePipe,
    PvfPipe,
    EmployeeInfoComponent,
  ],
  imports: [
    MatExpansionModule,
    CommonModule,
    EmployeeRoutingModule,
    LayoutModule,
    MatIconModule,
    MatListModule,
    MatCardModule
  ],
  providers: [
    EmployeeService,
  ]
})
export class EmployeeModule { }
