import { Component, Input, OnInit } from '@angular/core';
import { EmployeeModel } from 'src/app/model/employee.model';
import { PvfPipe } from 'src/app/pipe/pvf.pipe';
import { DateTool } from 'src/app/utils/datetool';
export enum EmployeeType {
  permanent = "Permanent",
  parttime = "Part-time"
}
type PvfInfo = {
  empCon: number;
  companyCon: number;
  totalEmpCon: number;
  totalComCon: number;
}
@Component({
  selector: 'app-employee-info',
  templateUrl: './employee-info.component.html',
  styleUrls: ['./employee-info.component.scss']
})
export class EmployeeInfoComponent implements OnInit {
  _info: EmployeeModel = {} as EmployeeModel;
  employeeType = EmployeeType;
  pvfInfo: PvfInfo = {} as PvfInfo;
  pvf_months: number = 0; // employee pvf month
  diffMonthWithToday = DateTool.diffMonthWithToday
  get info(): EmployeeModel {
    return this._info;
  }
  @Input() set info(value: EmployeeModel) {
    this._info = value;
    if(this._info) {
      this.pvf_months = this.calPvfMonth(this._info.startdate);
    }
  }
  constructor() { }

  ngOnInit(): void {

  }

  calPvfMonth = (startDate: string) => {
    const [day,month,year] = startDate.split("/");
    const date = new Date(+year,+month - 1,+day);
    date.setMonth(date.getMonth() + 3);
    return DateTool.diffMonthWithToday(date);
  }
  pvfContributionRate = (salary: number, rate: number,key: keyof PvfInfo) => {
    const pvf = new PvfPipe();
    this.pvfInfo[key] = pvf.transform(salary, rate);
    return this.pvfInfo[key]
  }

  pvfContributionInterest = (pmt: number) => {

    const rate = (2/100)/12 // rate per month
    return (pmt * ((Math.pow(1+rate,this.pvf_months)-1)/rate)) - (pmt * this.pvf_months);
  }

  pvfAccumulated = (pmt: number,key: keyof PvfInfo,reciveRate? : number) => {
    if(!reciveRate){
      reciveRate = 0;
      const n_year = this.pvf_months / 12;
      if(n_year > 5) {
        reciveRate = 100;
      } else if(n_year >= 3 && n_year < 5){
        reciveRate = 50;
      }
    }
    const rate = (2/100)/12 // rate per month
    this.pvfInfo[key] = (pmt * ((Math.pow(1+rate,this.pvf_months)-1)/rate)) * (reciveRate/100);
    return this.pvfInfo[key];
  }

}
