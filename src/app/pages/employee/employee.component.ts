import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { EmployeeModel } from '../../model/employee.model';
import { EmployeeService } from '../../services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],

})
export class EmployeeComponent implements OnInit {
  data: EmployeeModel[] = new Array<EmployeeModel>();
  constructor(
    private employee: EmployeeService,
  ) {

   }

  ngOnInit(): void {
    this.employee.get().subscribe(res => {
      console.log(res)
      this.data = res;
    });
  }
}
