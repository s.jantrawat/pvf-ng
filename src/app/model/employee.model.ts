export interface EmployeeModel {
  birthdate: string;
  employeeid: number;
  employeetype: string;
  firstname: string;
  lastname: string;
  pvfrate: number;
  salary: number;
  startdate: string;
  age: string;
}
