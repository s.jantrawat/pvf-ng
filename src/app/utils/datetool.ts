export  namespace DateTool {
  export const diffMonthWithToday = (date: string | Date): number => {
    const dateCount = diffDateWithToday(date);
    return Math.floor((dateCount / 365) * 12);
  }
  export const diffDateWithToday = (date: string | Date): number =>{
    const today = new Date();
    let sDate = new Date();
    const msPerday = 1000 * 3600 * 24;
    if(typeof date === 'string'){
      sDate = convertStrToDate(date);
    } else {
      sDate = date;
    }
    const diffTime = today.getTime() - sDate.getTime();
    const diffDate = diffTime / msPerday;
    return Math.abs(Math.floor(diffDate))
  }
  const convertStrToDate = (date:string): Date => {
      const [day,month,year] = date.split("/");
      return new Date(+year,+month - 1,+day);
  }
}
