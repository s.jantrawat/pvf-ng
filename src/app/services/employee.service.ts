import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EmployeeModel } from '../model/employee.model';
import * as EmployeeData from './data/Employees.json';
@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor() {}

  get(): Observable<EmployeeModel[]> {
    return new Observable((observer) => {
      const result= (EmployeeData as any).default;
      observer.next(result as EmployeeModel[]);
      observer.complete();
    });
  }
}
