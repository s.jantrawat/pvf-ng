import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'age'})
export class FindAgePipe implements PipeTransform {
  transform(value: string): number {
    const today = new Date();
    const [b_date,b_month,b_year] = value.split('/');
    let age = today.getFullYear() - parseInt(b_year,10);
    const m_left = today.getMonth() -  parseInt(b_month,10);
    if(m_left < 0 || (m_left === 0 && today.getDate() < parseInt(b_date))){
      age = age -1
    }
    return age;
  }
}
