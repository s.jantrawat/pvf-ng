import { Pipe, PipeTransform } from '@angular/core';
@Pipe({name: 'pvf'})
export class PvfPipe implements PipeTransform {
  transform(salary: number,rate:number): number {
    const count = salary * (rate/100)
    return count
  }
}
